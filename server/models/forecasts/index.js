import ForecastModel from './ForecastModel.js';
import ForecastType from './ForecastType.js';
import ForecastArgs from './ForecastArgs.js';

module.exports = {
  ForecastModel,
  ForecastType,
  ForecastArgs
};
